# Visual programming tools

Little collection of visual programming tools. Check the wikipedia page on [Visual programming language](https://en.wikipedia.org/wiki/Visual_programming_language) for more references.

## Sequential-block | scratch like

### Scratch
The most known one.

* **[GitHub](https://github.com/LLK/scratch-blocks)**
* **[Official web](https://scratch.mit.edu/developers)**

![Sample Scratch 3 programs](https://cloud.githubusercontent.com/assets/747641/15255731/dad4d028-190b-11e6-9c16-8df7445adc96.png)

[Sample screenshots of Scratch 3 programs on with horizontal and vertical blocks](https://github.com/LLK/scratch-blocks/#two-types-of-blocks)

### Blockly
The most common base to build other ones (like **Scratch**).

* **[GitHub](https://github.com/google/blockly)**
* **[Official web](https://developers.google.com/blockly/)**

![Screenshot of sample Blockly program](https://lh3.googleusercontent.com/-V3U254SXl9-Q0WD61bg8krSWqhYLC9SyKLsMRqVUHz_GGqEzo5mJCJf5dApo9X2-GVaDoncacSQZtBxocPr3Drd_BgT4w=s1352)

[Sample screenshot of Blockly program](https://developers.google.com/blockly/)

### Bitbloq
Blockly-based language for robots programming.

* **[GitHub](https://github.com/bq?utf8=%E2%9C%93&q=bitbloq&type=&language=)**
* **[Official web](https://bitbloq.bq.com/)**

![Sample BitBloq programming environment](https://soloelectronicos.files.wordpress.com/2016/04/pantalla1.png)

[Sample BitBloq programming environment](http://soloelectronicos.com/2016/04/14/el-robot-perfecto-para-aprender-y-divertirse/)

## Data-flow | node-red like

### Node-red
Well-known home automation tool.

* **[GitHub](https://github.com/node-red/node-red)**
* **[Official web](https://nodered.org/)**

![Screenshot of a node-red program](https://camo.githubusercontent.com/01ed64b01d73046a485ea82b645a3be529c64809/687474703a2f2f6e6f64657265642e6f72672f696d616765732f6e6f64652d7265642d73637265656e73686f742e706e67)

[Screenshot of a node-red program](https://github.com/node-red/node-red#node-red)

### Xible
Conceptually similar to node-red, but different considerations in regards to UX. [Read Xible's author comment on this.](https://www.reddit.com/r/programming/comments/az2un5/how_well_are_visual_automation_solutions_received/ei61988?utm_source=share&utm_medium=web2x)

* **[GitHub](https://github.com/spectrumbroad/xible)**
* **[Official web](https://xible.io/)**

![Screenshot of a sample Xible program](https://xible.io//img/showcase/math.add.png)

[Screenshot of a sample Xible program](https://xible.io/)


### Luna
A WYSIWYG language for data processing.

* **[GitHub](https://github.com/luna)**
* **[Official web](https://www.luna-lang.org/)**

![Screenshot of example luna program](https://cdn-images-1.medium.com/max/800/0*qHOSBP1gGYgCpiTw.)

[Screenshot of example luna program](https://medium.com/@luna_language/luna-the-future-of-computing-aaf4f76303ef)

### vvvv
Hybrid visual/text programming. With a long story. Core doesn't look like it's open.

* **[GitHub](https://github.com/vvvv/vvvv-sdk)**
* **[Official web](https://vvvv.org/)**

![Screenshot of vvvv's hybrid development environment](https://vvvv.org/sites/default/files/imagecache/large/images/hde.png)

[vvvv Hybrid development environment screenshot](https://vvvv.org/screenshots)

## Component connections

### XOD.io
A visual programming language for microcontrollers.

* **[GitHub](https://github.com/xodio/xod)**
* **[Official web](https://xod.io/)**

![Sample screenshot of a XOD.io program](https://xod.io/static/assets/5_add_pump.png)

[Screenshot of a sample XOD.io program](https://xod.io/)
