# Text manipulation tools

## Text analysis

Interesting tools for text analysis:

* [Infranodus](https://infranodus.com/): Web tool to explore a text as a graph.


## Text cleaning

* ftfy [[GitHub](https://github.com/LuminosoInsight/python-ftfy)]: Just fixes broken UTF-8


## Conversion

* pandoc [[GitHub](https://github.com/jgm/pandoc)][[official web](https://pandoc.org)]: Converts everything to everything!
