# Presentation software

Interesting software to build presentations:

## Text-based tools

* mdp [[on GitHub](https://github.com/visit1985/mdp)]: A command-line based markdown presentation tool.
* emacs + org-mode + reveal mode [[on GitHub](https://github.com/yjwen/org-reveal)]: A tool to convert org-mode files into [reveal.js](https://revealjs.com/) HTML presentations.
