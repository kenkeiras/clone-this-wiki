# Diagramming tools

Interesting tools for building diagrams:

## DSL based

These tools take as input a text file with a description of the diagram to produce and generate an image as output.

* Graphviz [[official web](https://graphviz.org)]: Simple but powerfull tool for graph visualization
* PlantUML [[official web](http://plantuml.com/)]: Tool to build UML diagrams, wireframes, Gantt diagrams, mind maps, ...
* Ditaa    [[official web](http://ditaa.sourceforge.net/)]: Build general diagrams from ascii-art.
* Mermaid [[official web](https://mermaidjs.github.io/)]: Visualize graphs, sequence, Gantt, class and git diagrams.

## GUI based

Tools to build diagrams through drag & drop.

* [Draw.io](https://draw.io): Generalistic web tool, has an electron-based desktop version.

