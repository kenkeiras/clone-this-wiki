In no particular order:

# Sources

* [Academic Torrents](http://academictorrents.com/)
* [Archive.org](https://archive.org/)
* [Kaggle](https://www.kaggle.com/datasets)
* [Awesome public datasets](https://github.com/caesar0301/awesome-public-datasets)
* [r/datasets](https://www.reddit.com/r/datasets)
* [DataHub](https://datahub.csail.mit.edu/www/)
* [Awesome data distribution dataset sources](https://github.com/amirouche/awesome-data-distribution#distribution)
* [data.world](https://data.world/): Requires sign up
* [Amazon public datasets](https://aws.amazon.com/opendata/public-datasets/): Requires passing an application
* [Google public datasets](https://cloud.google.com/public-datasets/): Accessed through BigQuery


# Datasets
* [Flickr1024: A Dataset for Stereo Image Super-resolution](https://yingqianwang.github.io/Flickr1024/) ([on Arxiv](https://arxiv.org/abs/1903.06332))
* [Middlebury Stereo datasets](http://vision.middlebury.edu/stereo/data/)
* [KITTI Vision Benchmark Suite data](http://www.cvlibs.net/datasets/kitti/raw_data.php)
* [ETH3D Benchmark datasets](https://www.eth3d.net/datasets)
